import {v4 as uuid} from 'uuid';
export class DestinoViaje{
    private selected: Boolean;
    servicios: String[];
    id = uuid();
    constructor(public nombre: String, public url: String){
        this.selected = false;
        this.servicios = ['pileta', 'desayuno']; 
    }
    

    isSelected(): Boolean{
        return this.selected;
    }

    setSelected(s: boolean){
        this.selected = s;
    }
}