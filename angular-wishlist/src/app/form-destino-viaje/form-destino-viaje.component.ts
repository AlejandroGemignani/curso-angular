import { Component, OnInit, Output, EventEmitter   } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators, ValidatorFn, AbstractControl} from '@angular/forms';
import { DestinoViaje } from '../models/destino-viaje.model';

@Component({
  selector: 'app-form-destino-viaje',
  templateUrl: './form-destino-viaje.component.html',
  styleUrls: ['./form-destino-viaje.component.css']
})
export class FormDestinoViajeComponent implements OnInit {
  
  @Output() onItemAdded: EventEmitter<DestinoViaje>;
  fg: FormGroup;
  minLongitud = 3;
  
  constructor(fb: FormBuilder) {
    this.onItemAdded = new EventEmitter;
    this.fg = fb.group({
      nombre: new FormControl('', Validators.compose([
       Validators.required,
       //this.nombreValidator,
       this.nombreValidatorParametrizable(this.minLongitud) 
      ])),
      url: new FormControl()
    });
    
    this.fg.valueChanges.subscribe((form: any) => {
      console.log('Cambió el formulario: ', form);
    });
  }
  
  ngOnInit(): void {
  }

  guardar(nombre: String, url: String): boolean{
    const d = new DestinoViaje(nombre, url);
    this.onItemAdded.emit(d);
    return false;
  }

  nombreValidator(control: FormControl): { [s: string]: boolean }{
    let l = control.value.toString().trim().length;
    if(l>0 && l<5){
      return { invalidNombre: true};
    }
    return { invalidNombre: false};
  }

  nombreValidatorParametrizable(minLong: number): ValidatorFn {
    return (control: AbstractControl): { [s: string]: boolean; } | null => {
      const l = control.value.toString().trim().length;
      if (l > 0 && l < minLong) {
        return { minLongNombre: true };
      }
      return null;
    }
  }

}
